# # CreatePostVoteRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**post_id** | **int** | Внешний ключ поста | [optional] 
**vote_type** | **int** | Тип голоса | [optional] 
**user_id** | **int** | Внешний ключ пользователя | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



# # CreatePostRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **string** | Заголовок поста | [optional] 
**content** | **string** | Содержимое поста | [optional] 
**user_id** | **int** | ID автора поста | [optional] 
**time_to_read** | **int** | Кол-во времени на прочтение в минутах | [optional] 
**tags** | **string[]** | Теги поста | [optional] 
**hubs** | **string[]** | Хабы поста | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



# heiz23\PostsServiceClient\VotesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPostVote**](VotesApi.md#createPostVote) | **POST** /posts/votes | Create a post vote
[**deletePostVote**](VotesApi.md#deletePostVote) | **DELETE** /posts/votes/{id} | Delete a post vote
[**patchPostVote**](VotesApi.md#patchPostVote) | **PATCH** /posts/votes/{id} | Update a post vote
[**searchPostVotes**](VotesApi.md#searchPostVotes) | **POST** /posts/votes:search | Search post votes



## createPostVote

> \heiz23\PostsServiceClient\Dto\PostVoteResponse createPostVote($create_post_vote_request)

Create a post vote

Create a post vote

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new heiz23\PostsServiceClient\Api\VotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_post_vote_request = new \heiz23\PostsServiceClient\Dto\CreatePostVoteRequest(); // \heiz23\PostsServiceClient\Dto\CreatePostVoteRequest | 

try {
    $result = $apiInstance->createPostVote($create_post_vote_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VotesApi->createPostVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_post_vote_request** | [**\heiz23\PostsServiceClient\Dto\CreatePostVoteRequest**](../Model/CreatePostVoteRequest.md)|  |

### Return type

[**\heiz23\PostsServiceClient\Dto\PostVoteResponse**](../Model/PostVoteResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deletePostVote

> \heiz23\PostsServiceClient\Dto\EmptyDataResponse deletePostVote($id)

Delete a post vote

Delete a post vote

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new heiz23\PostsServiceClient\Api\VotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deletePostVote($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VotesApi->deletePostVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\heiz23\PostsServiceClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchPostVote

> \heiz23\PostsServiceClient\Dto\PostVoteResponse patchPostVote($patch_post_votes_request)

Update a post vote

Update a post vote

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new heiz23\PostsServiceClient\Api\VotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$patch_post_votes_request = new \heiz23\PostsServiceClient\Dto\PatchPostVotesRequest(); // \heiz23\PostsServiceClient\Dto\PatchPostVotesRequest | 

try {
    $result = $apiInstance->patchPostVote($patch_post_votes_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VotesApi->patchPostVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **patch_post_votes_request** | [**\heiz23\PostsServiceClient\Dto\PatchPostVotesRequest**](../Model/PatchPostVotesRequest.md)|  |

### Return type

[**\heiz23\PostsServiceClient\Dto\PostVoteResponse**](../Model/PostVoteResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchPostVotes

> \heiz23\PostsServiceClient\Dto\SearchPostVotesResponse searchPostVotes($search_post_votes_request)

Search post votes

Search post votes

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new heiz23\PostsServiceClient\Api\VotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_post_votes_request = new \heiz23\PostsServiceClient\Dto\SearchPostVotesRequest(); // \heiz23\PostsServiceClient\Dto\SearchPostVotesRequest | 

try {
    $result = $apiInstance->searchPostVotes($search_post_votes_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VotesApi->searchPostVotes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_post_votes_request** | [**\heiz23\PostsServiceClient\Dto\SearchPostVotesRequest**](../Model/SearchPostVotesRequest.md)|  |

### Return type

[**\heiz23\PostsServiceClient\Dto\SearchPostVotesResponse**](../Model/SearchPostVotesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


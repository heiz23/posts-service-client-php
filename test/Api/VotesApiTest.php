<?php
/**
 * VotesApiTest
 * PHP version 5
 *
 * @category Class
 * @package  heiz23\PostsServiceClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Posts-service
 *
 * Posts-service
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the endpoint.
 */

namespace heiz23\PostsServiceClient;

use \heiz23\PostsServiceClient\Configuration;
use \heiz23\PostsServiceClient\ApiException;
use \heiz23\PostsServiceClient\ObjectSerializer;
use PHPUnit\Framework\TestCase;

/**
 * VotesApiTest Class Doc Comment
 *
 * @category Class
 * @package  heiz23\PostsServiceClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class VotesApiTest extends TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for createPostVote
     *
     * Create a post vote.
     *
     */
    public function testCreatePostVote()
    {
    }

    /**
     * Test case for deletePostVote
     *
     * Delete a post vote.
     *
     */
    public function testDeletePostVote()
    {
    }

    /**
     * Test case for patchPostVote
     *
     * Update a post vote.
     *
     */
    public function testPatchPostVote()
    {
    }

    /**
     * Test case for searchPostVotes
     *
     * Search post votes.
     *
     */
    public function testSearchPostVotes()
    {
    }
}

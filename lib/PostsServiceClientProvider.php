<?php

namespace heiz23\PostsServiceClient;

class PostsServiceClientProvider
{
    /** @var string[] */
    public static $apis = ['\heiz23\PostsServiceClient\Api\PostsApi', '\heiz23\PostsServiceClient\Api\VotesApi'];

    /** @var string[] */
    public static $dtos = [
        '\heiz23\PostsServiceClient\Dto\Error',
        '\heiz23\PostsServiceClient\Dto\SearchPostVotesRequest',
        '\heiz23\PostsServiceClient\Dto\EmptyDataResponse',
        '\heiz23\PostsServiceClient\Dto\CreatePostVoteRequest',
        '\heiz23\PostsServiceClient\Dto\PostResponse',
        '\heiz23\PostsServiceClient\Dto\PostVote',
        '\heiz23\PostsServiceClient\Dto\ModelInterface',
        '\heiz23\PostsServiceClient\Dto\RequestBodyPagination',
        '\heiz23\PostsServiceClient\Dto\PatchPostVotesRequest',
        '\heiz23\PostsServiceClient\Dto\SearchPostsRequest',
        '\heiz23\PostsServiceClient\Dto\SearchPostsResponseMeta',
        '\heiz23\PostsServiceClient\Dto\Post',
        '\heiz23\PostsServiceClient\Dto\PaginationTypeOffsetEnum',
        '\heiz23\PostsServiceClient\Dto\SearchPostsResponse',
        '\heiz23\PostsServiceClient\Dto\PostVoteFillableProperties',
        '\heiz23\PostsServiceClient\Dto\ErrorResponse',
        '\heiz23\PostsServiceClient\Dto\ErrorResponse2',
        '\heiz23\PostsServiceClient\Dto\ResponseBodyOffsetPagination',
        '\heiz23\PostsServiceClient\Dto\PaginationTypeEnum',
        '\heiz23\PostsServiceClient\Dto\SearchPostVotesResponse',
        '\heiz23\PostsServiceClient\Dto\PostVoteResponse',
        '\heiz23\PostsServiceClient\Dto\RequestBodyCursorPagination',
        '\heiz23\PostsServiceClient\Dto\PatchPostRequest',
        '\heiz23\PostsServiceClient\Dto\CreatePostRequest',
        '\heiz23\PostsServiceClient\Dto\PaginationTypeCursorEnum',
        '\heiz23\PostsServiceClient\Dto\PostFillableProperties',
        '\heiz23\PostsServiceClient\Dto\ResponseBodyCursorPagination',
        '\heiz23\PostsServiceClient\Dto\ResponseBodyPagination',
        '\heiz23\PostsServiceClient\Dto\RequestBodyOffsetPagination',
    ];

    /** @var string */
    public static $configuration = '\heiz23\PostsServiceClient\Configuration';
}

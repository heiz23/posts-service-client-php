<?php
/**
 * PaginationTypeOffsetEnum
 *
 * PHP version 5
 *
 * @category Class
 * @package  heiz23\PostsServiceClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Posts-service
 *
 * Posts-service
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace heiz23\PostsServiceClient\Dto;
use \heiz23\PostsServiceClient\ObjectSerializer;

/**
 * PaginationTypeOffsetEnum Class Doc Comment
 *
 * @category Class
 * @description * Pagination types: * &#x60;offset&#x60; - Пагинация используя offset
 * @package  heiz23\PostsServiceClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class PaginationTypeOffsetEnum
{
    public const OFFSET = 'offset';
    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public static function getAllowableEnumValues(): array
    {
        return [
            self::OFFSET,
        ];
    }

    /**
    * Gets allowable values and titles of the enum
    * @return string[]
    */
    public static function getDescriptions(): array
    {
        return [
            self::OFFSET => '',
        ];
    }
}


